cd $DEPLOY_PATH

mkdir -p "releases"

# move folder
if [ -d "releases/$CI_BUILD_ID" ]; then
	# rm -r "releases/$CI_BUILD_ID"
	rsync -av --delete "tmp/" "releases/$CI_BUILD_ID" && rm -r "tmp/"
else
	mv "tmp/" "releases/$CI_BUILD_ID"
fi

# set folder permission
chmod -R 775 "releases/$CI_BUILD_ID"

# symlink to shared path
echo "[+] creating symlinks..."
for f in $SHARED_PATHS
do
	# exclude=false
	# for ef in $EXCLUDE_PATHS
	# do
	# 	if [ $f = $ef ]; then
	# 		exclude=true
	# 		break
	# 	fi
	# done

	# if [ $exclude = false ]; then
		echo "shared/$f -> releases/$CI_BUILD_ID/$f"

		# create folders if not existing
		mkdir -p releases/$CI_BUILD_ID/$(dirname "$f") && chown -R "$CHOWN" releases/$CI_BUILD_ID/$(dirname "$f")
		mkdir -p shared/$(dirname "$f") && chown -R "$CHOWN" shared/$(dirname "$f")

		# copy repo files to shared if not existing yet
		if [[ -f "releases/$CI_BUILD_ID/$f" ]]; then
			cp -na releases/$CI_BUILD_ID/$f shared/$(dirname "$f")
			rm -rf releases/$CI_BUILD_ID/$f
		fi

		# create symlink or copy files
		if [[ -f "$(pwd)/shared/$f" && "$CP_SHARED_FILES" = true ]]; then
			cp -na $(pwd)/shared/$f releases/$CI_BUILD_ID/$f
		else
			rm -rf releases/$CI_BUILD_ID/$f
			ln -nfs $(pwd)/shared/$f releases/$CI_BUILD_ID/$f
		fi
	# fi
done

# symlink to public
ln -nfs releases/$CI_BUILD_ID/$SYMLINK_SRC_FOLDER $SYMLINK_DEST_FOLDER

# update permissions
echo "[+] update permissions..."
chown -R "$CHOWN" "releases/$CI_BUILD_ID"
# chown -R "$CHOWN" .

# remove all releases but the last
echo "[+] cleaning up..."
find releases/* ! -name "$CI_BUILD_ID" -maxdepth 0 -type d -exec rm -rf {} +

if [ "$REMOTE_SYNC" = true ]; then
	ENDSSH
fi

echo "[+] deployment successful!"