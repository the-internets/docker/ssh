#!/bin/bash
# @author Léo Chéron
# @site leo.cheron.works
# @email leo@cheron.works
#
# This script deploys the current repo to the remote server via SSH.


#------------------------------------------------------------------
# Arguments & env vars
#------------------------------------------------------------------

if [[ -z "${1}" || -z "${2}" ]]; then
	echo "usage: $(basename ${0}) <environment_file_path> <uid> [<private_key>]"
	echo ""
	echo "examples:"
	echo "  - $(basename ${0}) /path/to/.env.staging \$CI_BUILD_ID"
	exit 1
fi

SCRIPT_ROOT=$(dirname "$(readlink "$0" || echo "$0")")
CI_BUILD_ID=$2;
PRIVATE_KEY=$3;

SSH_PORT="${SSH_PORT:-22}"
DEPLOY_FROM="${DEPLOY_FROM:-public}"
SYMLINK_SRC_FOLDER="${SYMLINK_SRC_FOLDER:-}"
SYMLINK_DEST_FOLDER="${SYMLINK_DEST_FOLDER:-public}"

if [ ! -f ${1} ]; then
	echo "file ${1} does not exist. Aborting..."
	exit 1
fi

# Env vars
set -a
. ${1}
set +a

#------------------------------------------------------------------
# Deployment
#------------------------------------------------------------------

# add private key to authorize deployment
if [[ ! -z "$PRIVATE_KEY" && "$REMOTE_SYNC" = true ]]; then
	mkdir -p ~/.ssh

	# Write private key
	echo "[+] installing the private key..."
	echo "$PRIVATE_KEY" >> ~/.ssh/id_rsa
	chmod 400 ~/.ssh/id_rsa

	# Trust provided host
	echo "[+] adding $SSH_HOST:$SSH_PORT to trusted origins..."
	ssh-keyscan -p $SSH_PORT -t rsa -T 10 $SSH_HOST >> ~/.ssh/known_hosts
	chmod 444 ~/.ssh/known_hosts
fi

# upload repo to tmp folder
echo "[+] syncing to $SSH_HOST..."

if [ "$REMOTE_SYNC" = false ]; then
	mv $(pwd)/$DEPLOY_FROM/ $DEPLOY_PATH/tmp
	sh $SCRIPT_ROOT/deploy-exec.sh
else
	# rsync to remote
	for f in $EXCLUDE_PATHS
	do
		EXCLUDE="$EXCLUDE --exclude='$f'"
	done

	rsync -av -q --rsync-path="mkdir -p $DEPLOY_PATH/tmp && rsync" $(eval echo $EXCLUDE) -e "ssh -i ~/.ssh/id_rsa -p $SSH_PORT" $(pwd)/$DEPLOY_FROM/ ${SSH_USER}@${SSH_HOST}:$DEPLOY_PATH/tmp
	if [[ $? -gt 0 ]]; then
		echo "rsync failed..."
		exit 1
	fi

	# ssh login to remote server to finalize deployment
	ssh -i ~/.ssh/id_rsa -p $SSH_PORT ${SSH_USER}@${SSH_HOST} SHARED_PATHS=$(echo $SHARED_PATHS | sed -e 's/ /\\ /g') EXCLUDE_PATHS=$(echo $EXCLUDE_PATHS | sed -e 's/ /\\ /g') DEPLOY_PATH=$DEPLOY_PATH CI_BUILD_ID=$CI_BUILD_ID CHOWN=$CHOWN SYMLINK_SRC_FOLDER=$SYMLINK_SRC_FOLDER SYMLINK_DEST_FOLDER=$SYMLINK_DEST_FOLDER CP_SHARED_FILES=$CP_SHARED_FILES 'bash -s' < $SCRIPT_ROOT/deploy-exec.sh
fi